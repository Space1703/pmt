﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMT.Models
{
    public class Like
    {
        public Like() { }
        [Key]
        public int VoteId { get; set; }

        public int UserProfile_UserId { get; set; }

        [Required]
       
        public int CommentId { get; set; }
         [ForeignKey("CommentId")]
        public virtual Comment Comments { get; set; }
    }
}