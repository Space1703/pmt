﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMT.Models
{
    public class LoggedUser
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}