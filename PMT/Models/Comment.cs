﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace PMT.Models
{
    public class Comment
    {
        public Comment()
        {
            // Likes = new HashSet<Like>();
        }
        [Key]
        public int CommentId { get; set; }

        public string Message { get; set; }
        public int ObituaryId { get; set; }
        public string PostBy { get; set; }
        public int UserProfile_UserId { get; set; }
        public int LikeCount { get; set; }
        
        [ForeignKey("ObituaryId")]
        public virtual Obituary Obituary { get; set; }

        public virtual ICollection<Like> Likes { get; set; }
    }

    //public class CommentModel
    //{
    //    public int CommentId { get; set; }

    //    public string Message { get; set; }
    //    public int ObituaryId { get; set; }
    //    public string PostBy{get;set;}
    //    public int UserProfile_UserId { get; set; }
    //    [ForeignKey("ObituaryId")]
    //    public virtual Obituary Obituary { get; set; }

    //}
}
