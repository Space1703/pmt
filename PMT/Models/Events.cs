﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMT.Models
{
    public class Events
    {

        public Events() { }
        [Key]
        public int EventId { get; set; }
        public string EventName { get; set; }
        [Required]
        public string EventDate { get; set; }
        public string EventInfo { get; set; }
        [Required]
        public string EventVenue { get; set; }
        [Required]
        public string EventStartTime { get; set; }
        public string EventEndTime { get; set; }
        public string ApprximateDuration { get; set; }
        public int UserProfile_UserId { get; set; }
        public int ObituaryId { get; set; }
        //public int UserId { get; set; }
         [ForeignKey("ObituaryId")]
        public virtual Obituary Obituary { get; set; }

    }
}