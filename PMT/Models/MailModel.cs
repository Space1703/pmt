﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PMT.Models
{
    public class MailModel
    {
        //public string Email { get; set; }
        //public string Password { get; set; }
        [Key]
        public int MailId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
