﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace PMT.Models
{
    public class PMTContext : DbContext
    {
        public PMTContext()
            : base("PMTContext")
        {
            Database.SetInitializer(new PayMyTributeDb());
        }
        public DbSet<Obituary> Obituary { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Like> Likes { get; set; }
        //public DbSet<webpages_Membership> webpages_Membership { get; set; }
        public DbSet<ExternalUser> ExternalUsers { get; set; }
        public DbSet<MailModel> SendMails { get; set; }
        
    }

    public class PayMyTributeDb : DropCreateDatabaseIfModelChanges<PMTContext>
    {
    }
    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }

        public virtual ICollection<Obituary> Obituary { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Events> Events { get; set; }

    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }

        [Display(Name = "Your Email")]
        public string Email { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }


        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }

    public class ExternalUser
    {
        public int  Id { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
    }
    
}