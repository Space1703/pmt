﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PMT.Models
{
    public class Obituary
    {
        public Obituary()
        {
            Comments = new HashSet<Comment>();

            Events = new HashSet<Events>();
        }
        [Key]
        public int ObituaryId { get; set; }

        public string Classifications { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string ObituaryCategory { get; set; }
        public string DeacesedCity { get; set; }
        public string State { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime DateOfDeath { get; set; }
        [Required]
        public string Message { get; set; }
        public string FileName { get; set; }
        //public string FileSize { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserProfile UserProfile { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Events> Events { get; set; }
    }
}