﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMT.Models
{
    public class Search
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string ObituaryCategory { get; set; }
        public string DeacesedCity { get; set; }
        public string State { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? DateOfDeath { get; set; }
        public string Message { get; set; }
        public string TimeLine { get; set; }
        public string Classifications { get; set; }
    }
}