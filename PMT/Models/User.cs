﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace PMT.Models
{
    public class User
    {
        public User()
        {
            Comments = new HashSet<Comment>();

            Events = new HashSet<Events>();

            Obituary = new HashSet<Obituary>();


        }

        public int UserId { get; set; }

        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<Obituary> Obituary { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Events> Events { get; set; }


    }
}

