﻿using PMT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PMT.Utility
{
    public class commonServices
    {
        private PMTContext db = new PMTContext();
        public static LoggedUser GetUserByName(string UserName)
        {
            LoggedUser LoggedInUser = new LoggedUser();
            PMTContext db = new PMTContext();
            var user= db.UserProfiles.Where(a => a.UserName == UserName).FirstOrDefault();
            if (user != null)
            {

                LoggedInUser.UserId = user.UserId;
                LoggedInUser.UserName = user.UserName;

            }
            return LoggedInUser;
        }
    }
}