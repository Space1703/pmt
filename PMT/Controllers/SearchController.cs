﻿using PMT.Models;
using PMT.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace PMT.Controllers
{
    public class SearchController : AppController
    {
        //
        // GET: /Search/
        private PMTContext db = new PMTContext();
        public ActionResult Index()
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "--Select--", Value = " " });
            li.Add(new SelectListItem { Text = "Past 3 Days", Value = "Past 3 Days" });
            li.Add(new SelectListItem { Text = "Past 2 weeks", Value = "Past 2 weeks" });
            li.Add(new SelectListItem { Text = "Past 30 days", Value = "Past 30 days" });
            li.Add(new SelectListItem { Text = "Past 6 months", Value = "Past 6 months" });
            ViewData["TimeLine"] = li;
           
            List<SelectListItem> lis = new List<SelectListItem>();
            lis.Add(new SelectListItem { Text = "Select", Value = "" });
            lis.Add(new SelectListItem { Text = "Adult", Value = "Adult" });
            lis.Add(new SelectListItem { Text = "Child", Value = "Child" });
            ViewData["ObituaryCategory"] = lis;

            List<SelectListItem> list = new List<SelectListItem>();
            lis.Add(new SelectListItem { Text = "Select", Value = "" });
            lis.Add(new SelectListItem { Text = "Obituary", Value = "Obituary" });
            lis.Add(new SelectListItem { Text = "Triutes", Value = "Triutes" });
            ViewData["Classifiactions"] = list;

            return View();
        }

        [HttpPost]
        public ActionResult Search(Search searchModel)
        {
            //Search obi = new Search();
            //var result = (from search in db.Obituary
            //            where search.Name == Obituary.Name || search.LastName==Obituary.LastName
            //            select search ).ToList();


          

           var filterList = new List<SearchFilter>();
           if (!string.IsNullOrWhiteSpace(searchModel.DeacesedCity))
           {
               filterList.Add(new SearchFilter { PropertyName = "DeacesedCity", Value = searchModel.DeacesedCity, Operation = Op.Contains });
              
           }

           if (!string.IsNullOrWhiteSpace(searchModel.Name))
           {
               filterList.Add(new SearchFilter { PropertyName = "Name", Value = searchModel.Name, Operation = Op.Contains });

           }
           if (!string.IsNullOrWhiteSpace(searchModel.State))
           {
               filterList.Add(new SearchFilter { PropertyName = "State", Value = searchModel.State, Operation = Op.Contains });

           }

           if (!string.IsNullOrWhiteSpace(searchModel.LastName))
           {
               filterList.Add(new SearchFilter { PropertyName = "LastName", Value = searchModel.LastName, Operation = Op.Contains });

           }

           ////if (!string.IsNullOrWhiteSpace((searchModel.DateOf)))
           //{
           //    filterList.Add(new SearchFilter { PropertyName = "Name", Value = searchModel.DateOfBirth, Operation = Op.Contains });

           //}

           if (searchModel.DateOfBirth.HasValue)
           {
               filterList.Add(new SearchFilter { PropertyName = "DateOfBirth", Value = searchModel.DateOfBirth });
           }


           if (searchModel.DateOfDeath.HasValue )
           {
               filterList.Add(new SearchFilter { PropertyName = "DateOfDeath", Value = searchModel.DateOfDeath });
           }

           if (searchModel.TimeLine != null)
           {
               var dod = new DateTime();
               var timeline = searchModel.TimeLine;

               switch (timeline)
               {
                   case "Past 3 Days":
                       dod = DateTime.Today.AddDays(-3);
                       filterList.Add(new SearchFilter { PropertyName = "DateOfDeath", Value = dod, Operation = Op.GreaterThanOrEqual });
                       break;
                   case "Past 2 weeks":
                       dod = DateTime.Today.AddDays(-14);
                       filterList.Add(new SearchFilter { PropertyName = "DateOfDeath", Value = dod, Operation = Op.GreaterThanOrEqual });
                       break;
                   case "Past 30 days":
                       dod = DateTime.Today.AddDays(-30);
                       filterList.Add(new SearchFilter { PropertyName = "DateOfDeath", Value = dod, Operation = Op.GreaterThanOrEqual });
                       break;
                   case "Past 6 months":
                       dod = DateTime.Today.AddDays(-180);
                       filterList.Add(new SearchFilter { PropertyName = "DateOfDeath", Value = dod, Operation = Op.GreaterThanOrEqual });
                       break;
                   default:
                       break;
               }



               if (searchModel.Classifications!=null)
               {
                   var classfications = searchModel.Classifications;
                   switch(classfications)
                   {
                       case "Obituary":
                           filterList.Add(new SearchFilter { PropertyName = "Classifications", Value = searchModel.Classifications, Operation = Op.Contains });
                           break;
                       case "Tribute":
                           filterList.Add(new SearchFilter { PropertyName = "Classifications", Value = searchModel.Classifications, Operation = Op.Contains });
                           break;
                       default:
                           break;
                   }
               }







           }
















           var deleg = ExpressionBuilder.GetExpression<Obituary>(filterList).Compile();
           var filteredCollection = db.Obituary.Where(deleg).ToList();

           //filteredCollection = db.Obituary.Where(d => d.DeacesedCity.Contains(searchModel.DeacesedCity)).ToList();
          
            //var dod = new DateTime();
            //var timeline = searchModel.TimeLine;

            //switch (timeline)
            //{
            //    case "Past 3 Days":
            //        dod= DateTime.Today.AddDays(-3);
                       
            //        break;
            //    case "Past 2 weeks":
            //        dod = DateTime.Today.AddDays(-14);
                  
            //        break;
            //    case "Past 30 days":
            //        dod = DateTime.Today.AddDays(-30);
                    
            //        break;
            //    case "Past 6 months":
            //        dod = DateTime.Today.AddDays(-180);

            //        break;
            //    default:
            //        break;
            //}



            //var date = dod;



            //var search = db.Obituary.Where(s => s.Name == searchModel.Name || s.LastName == searchModel.LastName ||
            //                              s.ObituaryCategory == searchModel.ObituaryCategory || s.DateOfBirth == (searchModel.DateOfBirth.HasValue?searchModel.DateOfBirth.Value:default(DateTime))||
            //                              s.DateOfDeath == (searchModel.DateOfDeath.HasValue ? searchModel.DateOfDeath.Value : default(DateTime)) || s.DeacesedCity == searchModel.DeacesedCity ||
            //                              s.State == searchModel.State || s.DateOfDeath >= date).ToList();
            ////var search = (from s in db.Obituary
            ////              where (s.Name == Obituary.Name) && (s.LastName == Obituary.LastName)
            ////                     && (s.ObituaryCategory == Obituary.ObituaryCategory) && (s.DateOfBirth == Obituary.DateOfBirth)
            ////                     && (s.DateOfDeath == Obituary.DateOfDeath) && (s.DeacesedCity == Obituary.DeacesedCity)
            ////                     && (s.State == Obituary.State)
            ////              select s).ToList();
            //if (DateOfBirth != searchModel.DateOfBirth && DateOfDeath != searchModel.DateOfDeath)
            //{

            //}
           if (filteredCollection.Count() > 0)
            {
                return View(filteredCollection);
            }
            else
            {
                ViewBag.Message = "No Such Results found";
                return View(new List<Obituary>());
            }

           }


        public ActionResult Details(int id = 0)
        {
            var search = db.Obituary.Find(id);
            if (search == null)
            {
                return HttpNotFound();
            }
            // Comment co= db.Comments.Where(c => c.ObituaryId == id).ToList();
            //ViewBag.comment = co;
            return View(search);
        }


        [Authorize]
        public ActionResult Edit(int id = 0)
        {
            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Select", Value = "" });
            li.Add(new SelectListItem { Text = "Obituary", Value = "Obituary" });
            li.Add(new SelectListItem { Text = "Tribute", Value = "Tribute" });
            ViewData["Classifications"] = li;




            List<SelectListItem> lis = new List<SelectListItem>();
            lis.Add(new SelectListItem { Text = "Select", Value = "" });
            lis.Add(new SelectListItem { Text = "Adult", Value = "Adult" });
            lis.Add(new SelectListItem { Text = "Child", Value = "Child" });
            ViewData["ObituaryCategory"] = lis;


            Obituary obituary = db.Obituary.Find(id);
            if (obituary == null)
            {
                return HttpNotFound();
            }

            return View(obituary);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Obituary obituary)
        {

            obituary.UserId = (int)WebSecurity.CurrentUserId;

            if (ModelState.IsValid)
            {
                db.Entry(obituary).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", obituary.UserId);
            return View(obituary);
        }



    }
}
