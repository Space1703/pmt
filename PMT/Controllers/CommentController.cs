﻿using PMT.Filters;
using PMT.Helper;
using PMT.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace PMT.Controllers
{
    [InitializeSimpleMembership]
    public class CommentController : Controller
    {
        //
        // GET: /Comment/
        private PMTContext db = new PMTContext();
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Create()
        {

            return View();

        }

        //public ActionResult comments()
        //{
        //    ViewBag.PostId = 1;
        //    ViewBag.UserId = 1;
        //    return View("Create");
        //}

        // POST: /Post/Create

        //[HttpPost]
        //public ActionResult comments()
        //{

        //    return View("Create");
        //}
        [HttpPost]
        [Authorize]
        public ActionResult Create(Comment comment, string classification)
        {
            //var loggedUser=HttpContext.Session["UserData"] as LoggedUser;

            //  comment.UserProfile_UserId = loggedUser.UserId;

            var post = db.Obituary.Where(o => o.ObituaryId == comment.ObituaryId).SingleOrDefault();

            if (post != null)
            {
                comment.UserProfile_UserId = (int)WebSecurity.CurrentUserId;
                comment.PostBy = WebSecurity.CurrentUserName;

                db.Comments.Add(comment);
                db.SaveChanges();

            }




            //var query = db.UserProfiles.Where(up => up.UserId == comment.UserProfile_UserId).First();
            //ViewBag.UserName = query.UserName;
            var dataList = db.Comments.Where(c => c.CommentId == comment.CommentId).ToList();
            //List<CommentModel> newList = new List<CommentModel>();
            //foreach (var item in dataList) {
            //    CommentModel model = new CommentModel();
            //    model.Message = item.Message;
            //    model.PostBy = query.UserName;
            //    newList.Add(model);
            //}
            //  return Json(comment, JsonRequestBehavior.AllowGet);









            return Json(new { PartialView = MvcHelper.RenderPartialView(this, "_Index", dataList, null) });

        }

        // GET: /Post/Edit/5
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            Comment comment = db.Comments.Find(id);

            if (comment == null)
            {
                return HttpNotFound();
            }
            return Json(new { PartialView = MvcHelper.RenderPartialView(this, "_Edit", comment, null) }, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Comment comment)
        {
            //if (ModelState.IsValid)
            //{
            Comment editComment = db.Comments.Where(c => c.CommentId == comment.CommentId).SingleOrDefault();
            editComment.Message = comment.Message;
            db.Entry(editComment).State = EntityState.Modified;
            db.SaveChanges();

            //return RedirectToAction("Obituary_List", "Post", new { area = "" });
            //}

            return Json(new { comment = comment.Message });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment != null)
            {
                db.Comments.Remove(comment);
                db.SaveChanges();
                return Json(comment, JsonRequestBehavior.AllowGet);
            }
            return Json(db.Comments.Count(), JsonRequestBehavior.AllowGet);
        }


        [HttpGet]

        public ActionResult GetCommentCount(Obituary obituary)
        {
            var comm = db.Comments.Where(c => c.ObituaryId == obituary.ObituaryId).ToList().Count();

            return Json(comm);
        }





        [HttpPost]
        public ActionResult Like(Comment comment)
        {
            //like.Comments = db.Comments.Find(comment.CommentId);

            var loggedinUser = WebSecurity.CurrentUserId;

            var like = db.Likes.Where(l => l.CommentId == comment.CommentId && l.UserProfile_UserId == loggedinUser).SingleOrDefault();

            var commentLike = db.Comments.Where(c => c.CommentId == comment.CommentId).SingleOrDefault();

            if (like == null)
            {
                Like likes = new Like();
                likes.CommentId = comment.CommentId;
                likes.UserProfile_UserId = loggedinUser;
                if (commentLike != null)
                {
                    commentLike.LikeCount += 1;
                }
                db.Likes.Add(likes);
                db.SaveChanges();
            }

            //var userlike = db.Likes.Where(l => l.UserProfile_UserId == WebSecurity.CurrentUserId && l.CommentId == comment.CommentId);

            //db.CommentLikes.Add(like);
            //db.SaveChanges();
            //var likes = db.CommentLikes.Count();
            return Json(new { commentLike = commentLike.LikeCount }, JsonRequestBehavior.AllowGet);
        }












    }
}
