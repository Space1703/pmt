﻿using PMT.Filters;
using PMT.Models;
using PMT.Utility;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace PMT.Controllers
{
    [InitializeSimpleMembership]
    public class PostController : AppController
    {
        private PMTContext db = new PMTContext();

        //
        // GET: /Post/
        [HttpGet]

        public ActionResult Details(int id = 0)
        {
            Obituary obituary = db.Obituary.Find(id);
            if (obituary == null)
            {
                return HttpNotFound();
            }

            //ViewBag.comment = co;
            return View(obituary);
        }

        //
        // GET: /Post/Create
        [Authorize]
        public ActionResult Create()
        {

            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Obituary", Value = "Obituary" });
            li.Add(new SelectListItem { Text = "Tribute", Value = "Tribute" });
            ViewBag.classifications = li;
            

            List<SelectListItem> lis = new List<SelectListItem>();
            lis.Add(new SelectListItem { Text = "Select", Value = "" });
            lis.Add(new SelectListItem { Text = "Adult", Value = "Adult" });
            lis.Add(new SelectListItem { Text = "Child", Value = "Child" });
           
            ViewData["ObituaryCategory"] = lis;

            return View();
        }

        //
        // POST: /Post/Create






        public ActionResult Obituary_List(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.message = message;
            }
            var obituaries = db.Obituary.Where(x => x.Classifications=="Obituary");
            return View(obituaries);
        }


        public ActionResult Tribute_List(string message)
        {
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.message = message;
            }
            var tributes = db.Obituary.Where(x => x.Classifications == "Tribute");
            return View(tributes.ToList());
        }



        [HttpGet]
        public ActionResult Obituaries_Next(int lastObituary)   
        {
            List<Obituary> Obituaries = db.Obituary.OrderByDescending(a => a.ObituaryId).ToList();
            int last = 0;
            if (lastObituary == -1)
            {
                Obituaries = Obituaries.Take(6).ToList();
            }
            else if (lastObituary > 0)
            {
                Obituary obituaries = new Obituary();
                obituaries = Obituaries.Where(d => d.ObituaryId == lastObituary).SingleOrDefault();
                if (obituaries != null)
                {
                    int p = Obituaries.IndexOf(obituaries);
                    Obituaries = Obituaries.Skip(p + 1).Take(5).ToList();
                }
            }
            else if (lastObituary == 0)
            {
                Obituaries = null;
            }
            if (Obituaries.Count > 0 && Obituaries != null)
            {
                int len = Obituaries.Count;
                last = Obituaries.ElementAt(len - 1).ObituaryId;
            }

            ViewBag.Obituaries = Obituaries;
            ViewBag.lastObituary = last;
            return PartialView("_Obituary_List");
        }






        [HttpPost]
        public ActionResult Create(Obituary obituary, HttpPostedFileBase imageFile)
        {
            obituary.UserId = (int)WebSecurity.CurrentUserId;
            
                List<SelectListItem> li = new List<SelectListItem>();
                li.Add(new SelectListItem { Text = "Obituary", Value = "Obituary" });
                li.Add(new SelectListItem { Text = "Tribute", Value = "Tribute" });
                ViewBag.classifications = li;


                List<SelectListItem> lis = new List<SelectListItem>();
                lis.Add(new SelectListItem { Text = "Select", Value = "" });
                lis.Add(new SelectListItem { Text = "Adult", Value = "Adult" });
                lis.Add(new SelectListItem { Text = "Child", Value = "Child" });

                ViewData["ObituaryCategory"] = lis;

                var imageName = "";

                imageName = imageFile.FileName;
                var httpPostedFile = Request.Files[0];
                if (httpPostedFile != null)
                {
                    var uploadFilesDir = System.Web.HttpContext.Current.Server.MapPath("~/Images/obituary");
                    if (!Directory.Exists(uploadFilesDir))
                    {
                        Directory.CreateDirectory(uploadFilesDir);
                    }
                    var fileSavePath = Path.Combine(uploadFilesDir, httpPostedFile.FileName);

                    httpPostedFile.SaveAs(fileSavePath);
                    obituary.FileName = imageName;
                }

                if (ModelState.IsValid)
                {
                    
                    db.Obituary.Add(obituary);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home", new { message = obituary.Classifications + " " + "Added Successfully" });

                    ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", obituary.UserId);
                }
            return View(obituary);
        }
            
        //
        // GET: /Post/Edit/5
    [Authorize]
        public ActionResult Edit(int id = 0)
        {


            List<SelectListItem> li = new List<SelectListItem>();
            li.Add(new SelectListItem { Text = "Obituary", Value = "Obituary" });
            li.Add(new SelectListItem { Text = "Tribute", Value = "Tribute" });

            ViewBag.classifications = li;
           


            List<SelectListItem> lis = new List<SelectListItem>();
            lis.Add(new SelectListItem { Text = "Select", Value = "" });
            lis.Add(new SelectListItem { Text = "Adult", Value = "Adult" });
            lis.Add(new SelectListItem { Text = "Child", Value = "Child" });
            ViewData["ObituaryCategory"] = lis;


            Obituary obituary = db.Obituary.Find(id);
            if (obituary == null)
            {
                return HttpNotFound();
            }

            return View(obituary);
        }

        //
        // POST: /Post/Edit/5

  [HttpPost]
        public ActionResult Edit(Obituary obituary, HttpPostedFileBase imageFile)
        {

            obituary.UserId = (int)WebSecurity.CurrentUserId;

            if (ModelState.IsValid)
            {


                var imageName = "";
                if (imageFile != null)
                {
                    imageName = imageFile.FileName;
                    var httpPostedFile = Request.Files[0];
                    if (httpPostedFile != null)
                    {
                        var uploadFilesDir = System.Web.HttpContext.Current.Server.MapPath("~/Images/obituary");
                        if (!Directory.Exists(uploadFilesDir))
                        {
                            Directory.CreateDirectory(uploadFilesDir);
                        }
                        var fileSavePath = Path.Combine(uploadFilesDir, httpPostedFile.FileName);

                        httpPostedFile.SaveAs(fileSavePath);
                        obituary.FileName = imageName;
                    }
                }
                //db.Entry(obituary).State = EntityState.Modified;
                var currentPost=db.Obituary.Find(obituary.ObituaryId);

                db.Entry(currentPost).CurrentValues.SetValues(obituary);
                db.SaveChanges();
                if(obituary.Classifications=="Obituary")
                {
                    return RedirectToAction("Obituary_List", new { message = obituary.Classifications + " " + "Updated Successfully" });
                }
                else if (obituary.Classifications == "Tribute")
                {
                    return RedirectToAction("Tribute_List", new { message = obituary.Classifications + " " + "Updated Successfully" });
                }
                
            }
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", obituary.UserId);
            return View(obituary);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Obituary obituary = db.Obituary.Find(id);
            if (obituary == null)
            {
                return HttpNotFound();
            }
            return View(obituary);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Obituary obituary = db.Obituary.Find(id);
            db.Obituary.Remove(obituary);
            db.SaveChanges();
            return RedirectToAction("Obituary_List");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}