﻿using PMT.Helper;
using PMT.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebMatrix.WebData;

namespace PMT.Controllers
{
    public class EventController : Controller
    {
        //
        // GET: /Event/
        private PMTContext db = new PMTContext();
        public ActionResult Index()
        {
            return View();
        }
       [Authorize]
        public ActionResult Create(int postId)
        {

            ViewBag.PostId = postId;

            return View();
        }


        //public ActionResult Events()
        //{

        //    return View("Create");
        //}

        //  
        // POST: /Post/Create

        [HttpPost]
       public ActionResult Create(Events events)
        {
          //  events.ObituaryId = postId;
            var post = db.Obituary.Where(o => o.ObituaryId == events.ObituaryId).SingleOrDefault();
            if (ModelState.IsValid)
            {
                if (post != null)
                {
                    db.Events.Add(events);
                    db.SaveChanges();



                }
            }
            var datalist = db.Events.Where(c => c.EventId == events.EventId).ToList();
                
                //return RedirectToAction("Index", "Post", new { area = "" 
            //string erroreventVenue = "";
            //if (events.EventVenue == null)
            //{
            //    erroreventVenue = " error message!";
            //}
            return Json(new { PartialView = MvcHelper.RenderPartialView(this, "_Index", datalist, null) });
          

           
        }

        public ActionResult Edit(int id = 0)
        {
            Events events = db.Events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            
            return Json(new { PartialView = MvcHelper.RenderPartialView(this, "_Edit", events, null) }, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Events events)
        {
           //Events evnt=db.Events.Where(e => e.EventId == events.EventId).SingleOrDefault();
           //if (evnt != null)
           //{
           //    events.Obituary.UserId = evnt.Obituary.UserId;
           //}
               //db.Entry(events).State = EntityState.Modified;
                var currentEvent = db.Events.Find(events.EventId);
                events.UserProfile_UserId = WebSecurity.CurrentUserId;
                db.Entry(currentEvent).CurrentValues.SetValues(events);

                db.SaveChanges();

                var data = new
                {
                    name = events.EventName,
                    info = events.EventInfo,
                    venue = events.EventVenue,
                    date = events.EventDate,
                    duration = events.ApprximateDuration
                };
                //return RedirectToAction("Details",new RouteValueDictionary(new{contriller = "Event",id=events.EventId,message="Event Updarted!"}));
                return Json(data);
              
                //context..Attach(obj);
                ////context.ObjectStateManager.ChangeObjectState(obj, EntityState.Modified);
                ////context.tableentityname.ApplyCurrentValues(obj)
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // GET: /Post/Details/5
        [Authorize]
        public ActionResult Details(int id,string message)
        {
            Events events = db.Events.Find(id);
            if (!string.IsNullOrEmpty(message))
            {
                ViewBag.message = message;
            }
            if (events == null)
            {
                return HttpNotFound();
            }
            // Comment co= db.Comments.Where(c => c.ObituaryId == id).ToList();
            //ViewBag.comment = co;
            var post = db.Obituary.Where(c => c.ObituaryId == events.ObituaryId).SingleOrDefault();
            ViewBag.post = post.Classifications;
            return View(events);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Events events = db.Events.Find(id);
            if (events == null)
            {
                return HttpNotFound();
            }
            return View(events);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Events events = db.Events.Find(id);
            if (events != null)
            {
                db.Events.Remove(events);

                db.SaveChanges();
                return Json(events, JsonRequestBehavior.AllowGet);
            }
            return Json(db.Events.Count(), JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]

        //public ActionResult GetCommentCount(Obituary obituary)
        //{
        //    var comm = db.Events.Where(c => c.ObituaryId == obituary.ObituaryId).ToList().Count();

        //    return Json(comm);
        //}
    }
}
